class IssueImpactSearcher

  attr_reader :results

  PriorityLabels = ['P1', 'P2', 'P3', 'P4']
  SeverityLabels = ['S1', 'S2', 'S3', 'S4']
  GroupID = '9970' # gitlab-org group_id

  def initialize(team, type, token)
    @team = team
    @type = type
    @api_client = GitlabApi.new(token)
    @results = {}
  end

  def search
    PriorityLabels.product(SeverityLabels).each do |priority, severity|
      response = request([priority, severity])
      @results[[priority,severity]] = {
        count: response.headers['x-total'].to_i,
        url: "https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=#{@team}&label_name[]=#{@type}&label_name[]=#{severity}&label_name[]=#{priority}"
      }
    end
  end

  private

  def request(labels)
    @api_client.get("groups/#{GroupID}/issues", {
      scope: 'all',
      state: 'opened',
      labels: ([@team, @type] + labels).join(","),
      per_page: 1
    })
  end

end
