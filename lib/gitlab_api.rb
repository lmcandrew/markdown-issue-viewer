require 'faraday'
require 'faraday_middleware'

class GitlabApi

  Endpoint = 'https://gitlab.com/api/v4/'

  def initialize(token = nil)
    @token = token

    @connection = Faraday.new(:url => Endpoint) do |faraday|
      faraday.response :json
      faraday.adapter Faraday.default_adapter
    end
  end

  def get(path, params)
    @connection.get do |req|
      req.headers['Private-Token'] = @token if @token
      req.url path, params
    end
  end

end
