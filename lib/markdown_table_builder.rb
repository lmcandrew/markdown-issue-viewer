class MarkdownTableBuilder

  def initialize(title, columns:, rows:)
    @title = title
    @columns = columns
    @rows = rows
  end

  def build(data)
    header_row << seperator_row << body_rows(data)
  end

  private

  def header_row
    "|~#{@title}|" << @columns.map { |c| "~#{c}|" }.join << "\n"
  end

  def seperator_row
    "|--:|" << @columns.map { |_| ":-:|" }.join << "\n"
  end

  def body_rows(data)
    String.new.tap do |body|
      @rows.each do |row|
        body << "|~#{row}|"
        @columns.each do |column|
          cell = data[[row,column]]
          raise ArgumentError, "Cell #{row}, #{column} not present in data" unless cell
          body << "[#{cell[:count]}](#{cell[:url]})|"
        end
        body << "\n"
      end
    end
  end
end
