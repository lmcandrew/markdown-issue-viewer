require 'markdown_table_builder'

RSpec.describe MarkdownTableBuilder do
  describe '#build' do

    let(:columns) { ['S1', 'S2']}
    let(:rows) { ['P1', 'P2']}
    let(:markdown_table_builder) { described_class.new('mytable', columns:columns, rows:rows) }

    context 'when data set is complete' do

      let(:data) {
        {
          ['P1', 'S1'] => {count: 0, url: "http://www.example.com/0"},
          ['P1', 'S2'] => {count: 1, url: "http://www.example.com/1"},
          ['P2', 'S1'] => {count: 2, url: "http://www.example.com/2"},
          ['P2', 'S2'] => {count: 3, url: "http://www.example.com/3"},
        }
      }

      it "generates valid table" do

        expected = <<-'EXPECTED'
|~mytable|~S1|~S2|
|--:|:-:|:-:|
|~P1|[0](http://www.example.com/0)|[1](http://www.example.com/1)|
|~P2|[2](http://www.example.com/2)|[3](http://www.example.com/3)|
        EXPECTED

        expect(markdown_table_builder.build(data)).to eq(expected)
      end
    end

    context 'when data set is incomplete' do

      let(:data) {
        {
          ['P1', 'S2'] => {count: 1, url: "http://www.example.com/1"},
          ['P2', 'S1'] => {count: 2, url: "http://www.example.com/2"},
          ['P2', 'S2'] => {count: 3, url: "http://www.example.com/3"},
        }
      }

      it "raises an error" do
        expect { markdown_table_builder.build(data) }.to raise_error("Cell P1, S1 not present in data")
      end
    end

  end
end
