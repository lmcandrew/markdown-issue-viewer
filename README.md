# GitLab Priority Issue Viewer

Uses the GitLab API to count Issues by Priority/Severity and generates
a Markdown table with the results.

This is currently a manual script, which allows you to copy the output in to
a GitLab Issue.

![example](./markdown_issue_table.png)

Longer term it may be useful to automate this process.

## Running locally

```shell
$ bundle
$ bundle exec ruby run.rb # show options
```

The following command will return a Markdown table to stdout for all open bugs
belonging to the Manage team:

```shell
$ bundle exec ruby run.rb --type=bug --team=Manage --token="$GITLAB_API_TOKEN"
```

Optionally you can pipe to `pbcopy`, allowing you to paste the output
immediately in to GitLab:

```shell
$ bundle exec ruby run.rb --type=bug --team=Manage --token="$GITLAB_API_TOKEN" | pbcopy
```

## Contributing

Contributions are welcome! See [CONTRIBUTING.md] and the [LICENSE].

[CONTRIBUTING.md]: CONTRIBUTING.md
[LICENSE]: LICENSE
