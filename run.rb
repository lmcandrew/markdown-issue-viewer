require 'docopt'

require './lib/gitlab_api'
require './lib/issue_impact_searcher'
require './lib/markdown_table_builder'

docstring = <<DOCSTRING
Create a retrospective issue

Usage:
  #{__FILE__} --team=<team> --type=<type> --token=<token>
  #{__FILE__} -h | --help

Options:
  -h --help        Show this screen.
  --team=<team>    Team name, including initial caps (e.g. Manage)
  --type=<type>    Type of Issue (bug, security, technical-debt)
  --token=<token>  GitLab API token.
DOCSTRING

begin
  options = Docopt::docopt(docstring)

  team = options.fetch('--team')
  type = options.fetch('--type')
  token = options.fetch('--token')

  searcher = IssueImpactSearcher.new(team, type, token)
  searcher.search

  builder = MarkdownTableBuilder.new(type,
            columns: IssueImpactSearcher::SeverityLabels,
            rows: IssueImpactSearcher::PriorityLabels)
  puts builder.build(searcher.results)
rescue Docopt::Exit => e
  puts e.message
end
